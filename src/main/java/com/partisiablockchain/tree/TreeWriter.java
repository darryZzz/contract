package com.partisiablockchain.tree;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;

/**
 * Writes the avl tree.
 *
 * @param <K> the type of keys
 * @param <V> the type of values to associate with keys
 */
public interface TreeWriter<K extends Comparable<K>, V> {

  /**
   * Write an empty node to storage.
   *
   * @param hash the identifier to store the node under
   */
  void writeEmpty(HashAndSize hash);

  /**
   * Write a leaf node to storage.
   *
   * @param hash the identifier to store the node under
   * @param key the key stored in the leaf
   * @param value the value stored in the leaf
   */
  void writeLeaf(HashAndSize hash, K key, V value);

  /**
   * Write a composite node to storage.
   *
   * @param hash the identifier to store the node under
   * @param left the hash identifier of the left node in the composite
   * @param right the hash identifier of the right node in the composite
   * @param size the size of the tree rooted at this composite
   * @param height the height of the tree rooted at this composite
   */
  void writeComposite(HashAndSize hash, Hash left, Hash right, int size, byte height);

  /**
   * Calculate hash for an empty node.
   *
   * @return the calculated hash
   */
  HashAndSize hashEmpty();

  /**
   * Calculate hash for a leaf node.
   *
   * @param key the key stored in the leaf
   * @param value the value stored in the leaf
   * @return The calculated hash
   */
  HashAndSize hashLeaf(K key, V value);

  /**
   * Calculate hash for a composite node.
   *
   * @param left the hash identifier of the left node in the composite
   * @param right the hash identifier of the right node in the composite
   * @param size the size of the tree rooted at this composite
   * @param height the height of the tree rooted at this composite
   * @return The calculated hash
   */
  HashAndSize hashComposite(HashAndSize left, HashAndSize right, int size, byte height);
}
