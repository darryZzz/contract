package com.partisiablockchain.binder.sys;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.binder.BinderContract;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Binder contract for a system contract.
 *
 * @see com.partisiablockchain.contract.sys.SysContract
 */
public interface SysBinderContract<StateT extends StateSerializable>
    extends BinderContract<StateT, SysBinderContext, BinderEvent> {

  /**
   * Upgrades the state of a contract using the old contract.
   *
   * @param oldState the state of the previous contract
   * @param rpc the invocation data
   * @return state for the contract
   */
  StateT upgrade(StateAccessor oldState, byte[] rpc);
}
