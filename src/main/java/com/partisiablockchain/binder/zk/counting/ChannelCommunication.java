package com.partisiablockchain.binder.zk.counting;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeCollectionStream;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeFixedListStream;
import com.secata.tools.immutable.FixedList;
import java.util.Arrays;
import java.util.List;

/** Holds the amount of communication performed during an MPC. */
@Immutable
public final class ChannelCommunication implements StateSerializable, DataStreamSerializable {

  private static final SafeFixedListStream<Long> LIST_SERIALIZER =
      SafeFixedListStream.create(
          SafeDataInputStream::readLong,
          SafeCollectionStream.primitive(SafeDataOutputStream::writeLong));

  private final FixedList<Long> bytesSent;
  private final FixedList<Long> bytesReceived;

  @SuppressWarnings("unused")
  private ChannelCommunication() {
    this(null, null);
  }

  private ChannelCommunication(FixedList<Long> bytesSent, FixedList<Long> bytesReceived) {
    this.bytesSent = bytesSent;
    this.bytesReceived = bytesReceived;
  }

  /**
   * Creates a new ChannelCommunication based on the input.
   *
   * @param bytesSent the bytes sent
   * @param bytesReceived the bytes received
   * @return the created ChannelCommunication
   */
  public static ChannelCommunication create(List<Long> bytesSent, List<Long> bytesReceived) {
    return new ChannelCommunication(FixedList.create(bytesSent), FixedList.create(bytesReceived));
  }

  /**
   * Creates a new ChannelCommunication from two arrays.
   *
   * @param bytesReceived the bytes received
   * @param bytesSent the bytes sent
   * @return the created object
   */
  public static ChannelCommunication create(long[] bytesSent, long[] bytesReceived) {
    FixedList<Long> sentList = convert(bytesSent);
    FixedList<Long> receivedList = convert(bytesReceived);
    return new ChannelCommunication(sentList, receivedList);
  }

  private static FixedList<Long> convert(long[] bytesSent) {
    return FixedList.create(Arrays.stream(bytesSent).boxed());
  }

  /**
   * Reads a ChannelCommunication from the stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static ChannelCommunication read(SafeDataInputStream stream) {
    FixedList<Long> bytesSent = LIST_SERIALIZER.readFixed(stream, 3);
    FixedList<Long> bytesReceived = LIST_SERIALIZER.readFixed(stream, 3);
    return new ChannelCommunication(bytesSent, bytesReceived);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    LIST_SERIALIZER.writeFixed(stream, bytesSent);
    LIST_SERIALIZER.writeFixed(stream, bytesReceived);
  }

  /**
   * Get the lengths of bytes sent.
   *
   * @return lengths of bytes sent
   */
  public FixedList<Long> getBytesSent() {
    return bytesSent;
  }

  /**
   * Get the lengths of bytes received.
   *
   * @return lengths of bytes received
   */
  public FixedList<Long> getBytesReceived() {
    return bytesReceived;
  }
}
