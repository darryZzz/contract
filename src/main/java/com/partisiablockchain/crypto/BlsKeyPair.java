package com.partisiablockchain.crypto;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.crypto.bls.G1;
import com.partisiablockchain.crypto.bls.G2;
import com.partisiablockchain.crypto.bls.NafEncoded;
import java.math.BigInteger;

/** A BLS private/public key pair. */
public final class BlsKeyPair implements AggregateSignatureProvider {

  private final BlsPublicKey publicKey;
  private final NafEncoded nafEncodedSecretKey;
  private final BigInteger secretKey;

  /**
   * Construct a key BLS keypair given a secret key.
   *
   * @param secretKey the secret key
   */
  public BlsKeyPair(BigInteger secretKey) {
    this.secretKey = secretKey;
    this.nafEncodedSecretKey = NafEncoded.create(secretKey);
    G2 publicKey = G2.G.scalePoint(nafEncodedSecretKey);
    if (publicKey.isPointAtInfinity()) {
      throw new IllegalArgumentException("Public keys cannot point at infinity");
    }
    this.publicKey = new BlsPublicKey(publicKey);
  }

  /**
   * Get the BLS public key.
   *
   * @return BLS public key
   */
  public BlsPublicKey getPublicKey() {
    return publicKey;
  }

  /**
   * Get the private key.
   *
   * @return private key
   */
  @SuppressWarnings("WeakerAccess")
  public BigInteger getPrivateKey() {
    return secretKey;
  }

  /**
   * Sign a message using this private key.
   *
   * @param message the message to be signed
   * @return a new signature
   */
  @Override
  public BlsSignature sign(Hash message) {
    G1 hashedMessage = G1.hash(message);
    return new BlsSignature(hashedMessage.scalePoint(nafEncodedSecretKey));
  }
}
