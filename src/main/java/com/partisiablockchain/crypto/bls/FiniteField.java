package com.partisiablockchain.crypto.bls;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

interface FiniteField<T extends FiniteField<T>> {

  /**
   * Add two field elements.
   *
   * @param other the other element
   * @return the sum of this and other.
   */
  T add(T other);

  /**
   * Subtract two field elements.
   *
   * @param other the other element
   * @return the difference between the inputs.
   */
  T subtract(T other);

  /**
   * Compute the negated value of this.
   *
   * @return the additive inverse of this element.
   */
  T negate();

  /**
   * Multiply two field elements.
   *
   * @param other the other element
   * @return the product of the inputs.
   */
  T multiply(T other);

  /**
   * Square this element.
   *
   * @return this multiplied by itself.
   */
  T square();

  /**
   * Returns the inverse of this element.
   *
   * @return the inverse.
   */
  T invert();

  /**
   * Serialize this element into an array of bytes.
   *
   * @return a serialized version of this element.
   */
  byte[] serialize();

  /**
   * Test whether this element is the 0 element of the field.
   *
   * @return true if this is zero, and false otherwise.
   */
  boolean isZero();

  /**
   * Performs a lexicographic comparison of two field elements.
   *
   * @param other the other element
   * @return -1 if this is smaller than other, 0 if they're equal and 1 otherwise.
   */
  int compare(T other);

  /**
   * Computes the square root of this element.
   *
   * @return z such that z^2 == this.
   * @throws IllegalArgumentException if this value is not a square.
   */
  T sqrt();
}
