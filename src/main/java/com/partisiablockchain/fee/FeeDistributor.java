package com.partisiablockchain.fee;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.function.BiConsumer;

/** Distribute fees based on a list of weights, and merge them into a FeeDistribution. */
final class FeeDistributor {

  private FeeDistributor() {}

  /**
   * Distribute the gas based on the provided weights.
   *
   * @param gas the amount of gas to distribute
   * @param nodes the nodes the fees should be distributed to
   * @param weights the weights defining the distribution
   */
  static void distribute(
      long gas,
      FixedList<BlockchainAddress> nodes,
      FixedList<Integer> weights,
      BiConsumer<Long, BlockchainAddress> payFees) {
    if (nodes.size() != weights.size()) {
      throw new IllegalArgumentException(
          String.format(
              "The number of nodes (%d) does not match the number of weights (%d)",
              nodes.size(), weights.size()));
    }

    FixedList<Long> gasDistributions = distributeGas(gas, weights);
    for (int i = 0; i < nodes.size(); i++) {
      payFees.accept(gasDistributions.get(i), nodes.get(i));
    }
  }

  static FixedList<Long> distributeGas(long gas, FixedList<Integer> weights) {
    FixedList<Long> result = FixedList.create();
    long available = gas;

    BigInteger weightsSum = BigInteger.valueOf(weights.stream().reduce(0, Integer::sum));
    for (Integer w : weights) {
      BigInteger weight = BigInteger.valueOf(w);
      BigInteger scaledGas = weight.multiply(BigInteger.valueOf(available));
      long distribution = scaledGas.divide(weightsSum).longValue();
      result = result.addElement(distribution);
      weightsSum = weightsSum.subtract(weight);
      available -= distribution;
    }

    return result;
  }
}
