package com.partisiablockchain.fee;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.immutable.FixedList;

/** Pays fees for infrastructure work. */
public interface InfrastructurePayer {

  /**
   * Pay the target node for infrastructure work.
   *
   * @param gas the amount of gas to pay the node
   * @param target the node to pay
   */
  void payInfrastructureFees(long gas, BlockchainAddress target);

  /**
   * Pay the target nodes for infrastructure work. The gas will be distributed among the nodes
   * according to the supplied weights.
   *
   * @param gas the amount of gas to pay
   * @param nodes the nodes to pay
   * @param weights the weights defining how the gas should be divided among the nodes
   * @throws IllegalArgumentException if nodes.size() != weights.size()
   */
  default void payInfrastructureFees(
      long gas, FixedList<BlockchainAddress> nodes, FixedList<Integer> weights) {
    FeeDistributor.distribute(gas, nodes, weights, this::payInfrastructureFees);
  }
}
