package com.partisiablockchain.util;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Utility class for working with a fixed list.
 *
 * @param <T> the type of element in the lists.
 */
public final class ListUtil<T> {

  private final Supplier<T> emptyValue;
  private final Predicate<T> isEmpty;

  /**
   * Create list utility.
   *
   * @param emptyValue supplies an empty value
   * @param isEmpty checks if typed is empty
   */
  public ListUtil(Supplier<T> emptyValue, Predicate<T> isEmpty) {
    this.emptyValue = emptyValue;
    this.isEmpty = isEmpty;
  }

  /**
   * Create empty list utility.
   *
   * @param <T> type parameter for list
   * @return list utility
   */
  public static <T> ListUtil<T> createNull() {
    return new ListUtil<>(FunctionUtility.nullSupplier(), Objects::isNull);
  }

  /**
   * Create a new list with the new element inserted at the specified index. The list will be padded
   * with emptyValue to ensure the element is inserted at the proper location. Empty elements in the
   * tail of the list will be removed from the result.
   *
   * @param current the current list
   * @param index the index of the entry to update
   * @param newEntry the new entry
   * @return the updated list with the change entry
   */
  public FixedList<T> withEntry(FixedList<T> current, int index, T newEntry) {
    return withUpdatedEntry(current, index, t -> newEntry);
  }

  /**
   * Creates a new list where the element at index is set using the mapping function. The list will
   * be padded with emptyValue to ensure the element is inserted at the proper location. Empty
   * elements in the tail of the list will be removed from the result. The mapping function is
   * called with the current element at index or an empty element
   *
   * @param current the current list
   * @param index the index of the entry to update
   * @param mapping updater of the entry
   * @return the updated list with the change entry
   */
  public FixedList<T> withUpdatedEntry(FixedList<T> current, int index, Function<T, T> mapping) {
    ArrayList<T> result = new ArrayList<>(current);
    while (index >= result.size()) {
      result.add(emptyValue.get());
    }
    result.set(index, mapping.apply(result.get(index)));
    return pruneEmpty(result);
  }

  private FixedList<T> pruneEmpty(ArrayList<T> list) {
    for (int i = list.size() - 1; i >= 0; i--) {
      if (!isEmpty.test(list.get(i))) {
        return FixedList.create(list.subList(0, i + 1));
      }
    }
    return FixedList.create();
  }
}
