package com.partisiablockchain.serialization;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.OutputStream;
import java.util.function.Consumer;
import java.util.function.Function;

/** Decorator storage, counting number of bytes written. */
public final class CountingStorage implements StateStorage {

  private final StateStorage storage;
  private int writtenByteCount;

  CountingStorage(StateStorage storage) {
    this.storage = storage;
  }

  int writtenByteCount() {
    return writtenByteCount;
  }

  @Override
  public boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    boolean written = storage.write(hash, writer);
    if (written) {
      ExceptionConverter.run(
          () ->
              writer.accept(
                  new SafeDataOutputStream(
                      new OutputStream() {
                        @Override
                        public void write(int b) {
                          writtenByteCount++;
                        }
                      })),
          "Unable to count size");
    }
    return written;
  }

  @Override
  public <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
    return storage.read(hash, reader);
  }
}
