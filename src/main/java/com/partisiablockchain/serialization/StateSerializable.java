package com.partisiablockchain.serialization;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.google.errorprone.annotations.Immutable;

/**
 * Interface for marking classes as serializable by blockchain framework.
 *
 * <p>All implementors must be marked with {@link Immutable}, and additionally be either:
 *
 * <ul>
 *   <li>Classic class, with a no-args constructor.
 *   <li>Record class.
 * </ul>
 *
 * <p>The record case requires the {@code supportRecordDeserialization} feature toggle enabled when
 * deserializing. Attempting to deserialize records when set to false will result in the old
 * behaviour which will most likely result in reflection errors:
 *
 * <ul>
 *   <li>Empty record (without any fields) will successfully deserialize.
 *   <li>Non-empty records with empty constructor will throw {@link
 *       java.lang.IllegalAccessException}.
 *   <li>All other records will throw {@link java.lang.NoSuchMethodException}.
 * </ul>
 */
@Immutable
@CheckReturnValue
public interface StateSerializable {}
