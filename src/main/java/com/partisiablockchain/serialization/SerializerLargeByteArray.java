package com.partisiablockchain.serialization;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.serialization.SerializationContext.Serializer;
import com.secata.stream.SafeDataInputStream;

final class SerializerLargeByteArray implements Serializer<LargeByteArray> {

  private final HashAndSize hash;
  private final LargeByteArray largeByteArray;
  private final StateStorage storage;

  SerializerLargeByteArray(StateStorage storage, LargeByteArray largeByteArray) {
    this.storage = storage;
    this.largeByteArray = largeByteArray;
    this.hash = largeByteArray.hashWithSize();
  }

  SerializerLargeByteArray(StateStorage storage, Hash hash) {
    this.storage = storage;
    this.largeByteArray = null;
    this.hash = hash.withZeroSize();
  }

  @Override
  public HashAndSize hash() {
    return hash;
  }

  @Override
  public LargeByteArray read() {
    return new LargeByteArray(storage.read(hash.hash(), SafeDataInputStream::readDynamicBytes));
  }

  @Override
  public void write() {
    storage.write(hash.hash(), stream -> stream.writeDynamicBytes(largeByteArray.getData()));
  }
}
