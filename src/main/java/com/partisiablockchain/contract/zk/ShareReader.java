package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.secata.util.BitHelper;
import java.util.Objects;

/**
 * Utility class reads a byte array representation of a list of values. E.g. two integers followed
 * by five booleans and then a long is read from a byte array perceived as a consecutive bit array,
 * the first 8 bytes is allocated to the two integers, the next byte has five bit allocated to
 * booleans, the next 3 bits, plus the next 8 bytes is the long.
 *
 * @see ShareWriter
 */
public final class ShareReader {

  private final byte[] value;
  private final int shareBitLength;
  private int index = 0;

  ShareReader(byte[] value, int shareBitLength) {
    this.value = Objects.requireNonNull(value, "Value to read must be open");
    this.shareBitLength = shareBitLength;
    if (ZkClosed.getByteLength(shareBitLength) != value.length) {
      throw new IllegalArgumentException("Supplied open value and bit length are inconsistent");
    }
  }

  /**
   * Reads a long of the supplied bit length from the open value.
   *
   * @param bitLength the bit length of the long to read
   * @return the read long
   */
  public long readLong(int bitLength) {
    if (bitLength < 1 || bitLength > 64) {
      throw new IllegalArgumentException("Illegal bit length for long. Must be between 1 and 64.");
    }
    long result = 0;
    for (int i = 0; i < bitLength; i++) {
      if (readBoolean()) {
        result = result | (1L << i);
      }
    }
    return result;
  }

  /**
   * Reads a long of the supplied bit length from the open value, this method interprets the number
   * signed, so the last bit is extended to the remaining bits in the long. This makes the three bit
   * number 0b111 equal to -1 rather than 7.
   *
   * @param bitLength the bit length of the long to read
   * @return the read long
   */
  public long readSignedLong(int bitLength) {
    long result = readLong(bitLength);

    int signBitPlace = bitLength - 1;
    long sign = (result & 1L << signBitPlace) >> signBitPlace;
    for (int i = bitLength; i < Long.SIZE; i++) {
      result = result | sign << i;
    }
    return result;
  }

  /**
   * Reads a boolean from the open value.
   *
   * @return the read boolean
   */
  public boolean readBoolean() {
    if (this.index >= shareBitLength) {
      throw new IllegalStateException("No more bits available");
    }
    byte currentByte = value[this.index / 8];
    int bitIndex = this.index % 8;
    this.index++;
    return BitHelper.isLongBitSet(currentByte, bitIndex);
  }

  /**
   * Reads a byte array from the open value.
   *
   * @param byteCount the number of bytes to read
   * @return the read bytes
   */
  public byte[] readBytes(int byteCount) {
    byte[] result = new byte[byteCount];
    for (int i = 0; i < result.length; i++) {
      result[i] = (byte) readLong(8);
    }
    return result;
  }

  /**
   * Construct a share reader.
   *
   * @param value the value
   * @param shareBitLength the share bit length
   * @return a ShareReader
   */
  public static ShareReader create(byte[] value, int shareBitLength) {
    return new ShareReader(value, shareBitLength);
  }
}
