package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.Contract;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import java.util.List;

/**
 * Implementations of this class is considered valid zk contracts on the PBC platform.
 *
 * <p>The state of a ZK contract can be viewed in a hierarchy:
 *
 * <ul>
 *   <li>Contract information: OpenT
 *   <li>Variable1
 *       <ul>
 *         <li>Information: ZkOpenT
 *         <li>Value: ZkClosedT
 *       </ul>
 *   <li>Variable2
 *       <ul>
 *         <li>Information: ZkOpenT
 *         <li>Value: ZkClosedT
 *       </ul>
 * </ul>
 *
 * <p>Interactions from the user is processed in
 *
 * <ol>
 *   <li>{@link #onOpenInput}: Takes input from the invocation and can change the state of the
 *       contract, a typical example is the possibility to trigger the ZkComputation
 *   <li>{@link #onSecretInput} is the registration of intended secret input, before it actually is
 *       written to the state.
 *   <li>{@link #onUserVariablesOpened} happens when a user opens variables on the contract, meaning
 *       the secret values are exposed.
 * </ol>
 *
 * <p>Secret input is send to the smart contract and the nodes, that means the the processing
 * happens in two phases, first the registration, next the nodes acknowledges this.
 *
 * <ol>
 *   <li>{@link #onVariableInputted} is on variable confirmation, after it is written to the state.
 *   <li>{@link #onVariableRejected} rejects a variable, nothing is written to the state.
 * </ol>
 *
 * <p>Other interactions are (all done by the computing nodes):
 *
 * <ul>
 *   <li>{@link #onComputeComplete} when the computation is complete.
 *   <li>{@link #onVariablesOpened} when the contract opens a variable, this is handles by the
 *       nodes, so this is really the contract triggers a node interaction.
 * </ul>
 *
 * @param <OpenT> the type of the public state for the contract
 * @param <ZkOpenT> the type of the public state for each variable
 * @param <ZkClosedT> the variable type (depending on the underlying protocol)
 * @param <ComputationT> the type binding the execution of the zk computation
 */
public abstract class ZkContract<
        OpenT extends StateSerializable,
        ZkOpenT extends StateSerializable,
        ZkClosedT extends ZkClosed<ZkOpenT>,
        ComputationT>
    implements Contract {

  private final ComputationT computation;

  /**
   * Constructor with zk computation for inheritance.
   *
   * @param computation zk computation
   */
  protected ZkContract(ComputationT computation) {
    this.computation = computation;
  }

  /**
   * Get the zk computation.
   *
   * @return zk computation
   */
  public final ComputationT getComputation() {
    return computation;
  }

  /**
   * Creates the contract - comparable to a constructor. The current user is the owner of the
   * contract. Similarly to {@link PubContract#onCreate}.
   *
   * @param context the context for this execution
   * @param zkState the zero knowledge state
   * @param invocation the input data in this transaction
   * @return updated state for the contract
   */
  public OpenT onCreate(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      SafeDataInputStream invocation) {
    // NOOP
    return null;
  }

  /**
   * Callback registered in a previous event created in an invocation will be handled here.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state state for the contract
   * @param callbackContext the spawned event transactions and their execution result
   * @param rpc the invocation data registered with the callback
   * @return updated state for the contract
   */
  public OpenT onCallback(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      OpenT state,
      CallbackContext callbackContext,
      SafeDataInputStream rpc) {
    // NOOP
    return state;
  }

  /**
   * Any invocation to this contract that does not contain secret input is handled here.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param invocation the input data in this transaction
   * @return updated state for the contract
   */
  public OpenT onOpenInput(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      OpenT state,
      SafeDataInputStream invocation) {
    // NOOP
    return state;
  }

  /**
   * Any invocation to this contract containing sending secret input is handled here. This
   * invocation is the registration of intended secret input, before it actually is written to the
   * state. In this invocation additional input related to the secret input can be registered as
   * well as other state changes - in this invocation the contract programmer have a chance to
   * reject it.
   *
   * <p>It is important to call {@link SecretInputBuilder#expectBitLength} for the variable to
   * expect.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param input the builder enabling building a complete input
   * @param invocation the input data in this transaction
   * @return updated state for the contract
   */
  public OpenT onSecretInput(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      OpenT state,
      SecretInputBuilder<ZkOpenT> input,
      SafeDataInputStream invocation) {
    // NOOP
    return state;
  }

  /**
   * This invocation occurs by the computation nodes when secret input is confirmed. After this call
   * the input is now considered a part of the total state. In this invocation the contract
   * programmer can react to the input (for instance start computation if a sufficient amount of
   * input has been reached).
   *
   * <p>Sender of this transaction is a computation node - do not throw Exceptions during execution.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param variableId the variable id now inputted
   * @return updated state for the contract
   */
  public OpenT onVariableInputted(
      ZkContractContext context, ZkState<ZkOpenT, ZkClosedT> zkState, OpenT state, int variableId) {
    // NOOP
    return state;
  }

  /**
   * This invocation occurs by the computation nodes when secret input is rejected. This means that
   * the input from previous {@link #onSecretInput} is disregarded in the contract state
   *
   * <p>Sender of this transaction is a computation node - do not throw Exceptions during execution.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param variableId the variable id now inputted
   * @return updated state for the contract
   */
  public OpenT onVariableRejected(
      ZkContractContext context, ZkState<ZkOpenT, ZkClosedT> zkState, OpenT state, int variableId) {
    // NOOP
    return state;
  }

  /**
   * Called when the nodes have completed the {@link #getComputation() computation}.
   *
   * <p>Sender of this transaction is a computation node - do not throw Exceptions during execution.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param createdVariables the variable id now inputted
   * @return updated state for the contract
   */
  public OpenT onComputeComplete(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      OpenT state,
      List<Integer> createdVariables) {
    // NOOP
    return state;
  }

  /**
   * Called when a user have opened one or more variable.
   *
   * <p>The underlying transaction contained the data.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param openedVariables the variable ids which have been opened
   * @param invocation the input data in this transaction
   * @return updated state for the contract
   */
  public OpenT onUserVariablesOpened(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      OpenT state,
      List<Integer> openedVariables,
      SafeDataInputStream invocation) {
    // NOOP
    return state;
  }

  /**
   * Called upon the result of the contract having called {@link ZkState#openVariables}.
   *
   * <p>Sender of this transaction is a computation node - do not throw Exceptions during execution.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param openedVariables the variable ids which have been opened
   * @return updated state for the contract
   */
  public OpenT onVariablesOpened(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      OpenT state,
      List<Integer> openedVariables) {
    // NOOP
    return state;
  }

  /**
   * Called when a piece of data has been attested by enough computation nodes. Attestation on a
   * piece of data can be requested by calling {@link ZkState#attestData}, and this method on the
   * contract is called when the attestation process finishes.
   *
   * @param context context for the current invocation
   * @param zkState the zero knowledge state
   * @param state the open state for the contract
   * @param attestationId the ID of the attestation request
   * @return updated state for the contract
   */
  public OpenT onAttestationComplete(
      ZkContractContext context,
      ZkState<ZkOpenT, ZkClosedT> zkState,
      OpenT state,
      Integer attestationId) {
    // NOOP
    return state;
  }
}
