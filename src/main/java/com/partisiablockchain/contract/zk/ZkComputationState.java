package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * This state is passed to the computation as a parameter.
 *
 * <p>It contains accessors to the state in the {@link ZkContract} (the owner of the computation).
 *
 * <p>The state is divided into three parts:
 *
 * <ul>
 *   <li>The global state - similar to the state in {@link
 *       com.partisiablockchain.contract.pub.PubContract}
 *   <li>The user specific state - each user can have a state (e.g. block time to enter the smart
 *       contract)
 *   <li>the variable specific state - if a variable has some public part (a secret bid and a public
 *       commitment of this bid)
 * </ul>
 *
 * @param <OpenT> the open state of the contract
 * @param <ZkOpenT> the open state related to each variable
 * @param <ZkClosedT> the type of the closed variable - this is bound for sake of the computation
 */
public interface ZkComputationState<
    OpenT extends StateSerializable,
    ZkOpenT extends StateSerializable,
    ZkClosedT extends ZkClosed<ZkOpenT>> {

  /**
   * Gets the contract state that the computation is responsible for.
   *
   * @return the state
   */
  OpenT getOpenState();

  /**
   * Gets a variable with a specific id in the smart contract available in the computation.
   *
   * @param id the isd to lookup
   * @return the variable
   */
  ZkClosedT getVariable(int id);

  /**
   * Gets the variables in the smart contract available in the computation.
   *
   * @return the list of variables
   */
  Collection<ZkClosedT> getVariables();

  /**
   * Gets every confirmed closed variable for a specific account. Each variable will be included as
   * a variable in the computation.
   *
   * @param owner the user to look up variables for
   * @return the list of variables
   */
  default Collection<ZkClosedT> getVariables(BlockchainAddress owner) {
    return getVariables().stream()
        .filter(zkClosedT -> zkClosedT.getOwner().equals(owner))
        .collect(Collectors.toList());
  }
}
