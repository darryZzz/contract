package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Closed variable in the zk computation, this type is bound to the variable type in the concrete
 * protocol. However a secret variable shares a set of common features defined in this interface.
 *
 * @param <ZkOpenT> the open state related to the variable
 */
@Immutable
public interface ZkClosed<ZkOpenT extends StateSerializable> extends StateSerializable {

  /**
   * Get byte length from bit-length.
   *
   * @param bitLength to get byte length from
   * @return byte length
   */
  static int getByteLength(int bitLength) {
    return 1 + ((bitLength - 1) >> 3);
  }

  /**
   * Unique integer identification of this variable.
   *
   * @return the id of the variable
   */
  int getId();

  /**
   * Each variable is always owned by a single entity, this can be either a user or a contract.
   *
   * @return the owner of this variable.
   */
  BlockchainAddress getOwner();

  /**
   * Any information the variables in this contract holds which is publicly known.
   *
   * @return the open state of this variable
   */
  ZkOpenT getInformation();

  /**
   * Sealed variable is a variable that cannot be inspected - not event by the owner. Useful if the
   * owner is no longer the entity that knew the secret value of the variable.
   *
   * @return true if sealed.
   */
  boolean isSealed();

  /**
   * Gets the bytes for a variable that has been opened on the blockchain. Null if not opened.
   * Interpreting the bytes is protocol specific.
   *
   * @return the bytes or null.
   */
  byte[] getOpenValue();

  /**
   * Bit length of this variable - for binary sharing this is precise, bit length 32 means a regular
   * integer.
   *
   * @return how many bits in the variable
   */
  int getShareBitLength();

  /**
   * Create a reader for the open value of this ZkClosed. Requires that the variables has been
   * opened.
   *
   * @return a new reader for the open value of this closed
   */
  default ShareReader reader() {
    return new ShareReader(getOpenValue(), getShareBitLength());
  }
}
