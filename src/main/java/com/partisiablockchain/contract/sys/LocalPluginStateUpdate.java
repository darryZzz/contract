package com.partisiablockchain.contract.sys;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Class representing an update to be performed against the local state of a plugin. */
public final class LocalPluginStateUpdate implements DataStreamSerializable {

  private final BlockchainAddress context;
  private final byte[] rpc;

  private LocalPluginStateUpdate(BlockchainAddress context, byte[] rpc) {
    this.context = context;
    this.rpc = rpc;
  }

  /**
   * Create local plugin state update.
   *
   * @param context address for plugin
   * @param rpc invocation data
   * @return local plugin state update
   */
  public static LocalPluginStateUpdate create(BlockchainAddress context, byte[] rpc) {
    return new LocalPluginStateUpdate(context, rpc.clone());
  }

  /**
   * Get the address for plugin.
   *
   * @return address for plugin
   */
  public BlockchainAddress getContext() {
    return context;
  }

  /**
   * Get the invocation data.
   *
   * @return invocation data
   */
  public byte[] getRpc() {
    return rpc.clone();
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    context.write(stream);
    stream.writeDynamicBytes(rpc);
  }

  /**
   * Read local plugin state update from stream.
   *
   * @param stream to read from
   * @return read local plugin state update
   */
  public static LocalPluginStateUpdate read(SafeDataInputStream stream) {
    return new LocalPluginStateUpdate(
        BlockchainAddress.read(stream), DataStreamLimit.readDynamicBytes(stream));
  }
}
