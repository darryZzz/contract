package com.partisiablockchain.contract.sys;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** An update that is to be performed against the global state of a plugin. */
public final class GlobalPluginStateUpdate implements DataStreamSerializable {

  private final byte[] rpc;

  private GlobalPluginStateUpdate(byte[] rpc) {
    this.rpc = rpc;
  }

  /**
   * Create global plugin state update.
   *
   * @param rpc invocation data
   * @return global plugin state update
   */
  public static GlobalPluginStateUpdate create(byte[] rpc) {
    return new GlobalPluginStateUpdate(rpc.clone());
  }

  /**
   * Get the invocation data.
   *
   * @return invocation data
   */
  public byte[] getRpc() {
    return rpc.clone();
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeDynamicBytes(rpc);
  }

  /**
   * Read global plugin state update from stream.
   *
   * @param stream to read form
   * @return read global plugin state update
   */
  public static GlobalPluginStateUpdate read(SafeDataInputStream stream) {
    return new GlobalPluginStateUpdate(DataStreamLimit.readDynamicBytes(stream));
  }
}
