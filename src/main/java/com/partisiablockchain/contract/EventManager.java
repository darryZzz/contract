package com.partisiablockchain.contract;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.secata.stream.DataStreamSerializable;

/** EventManager used to create event transactions. */
public interface EventManager extends EventCreator {

  /**
   * Sets the data that the currently sending contract will be invoked with when the entire batch of
   * interactions has completed.
   *
   * @param rpc the data in the callback
   * @param allocatedCost the allocated cost for this callback
   */
  void registerCallback(DataStreamSerializable rpc, long allocatedCost);

  /**
   * Sets the data that the currently sending contract will be invoked with when the entire batch of
   * interactions has completed. This callback will be paid by a share of the remaining allocated
   * cost.
   *
   * @param rpc the data in the callback
   */
  void registerCallbackWithCostFromRemaining(DataStreamSerializable rpc);

  /**
   * Sets the data that the currently sending contract will be invoked with when the entire batch of
   * interactions has completed. This callback will be paid by the sending contract.
   *
   * @param rpc the data in the callback
   * @param allocatedCost the allocated cost for this callback
   */
  void registerCallbackWithCostFromContract(DataStreamSerializable rpc, long allocatedCost);
}
