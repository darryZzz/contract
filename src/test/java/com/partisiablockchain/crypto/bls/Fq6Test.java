package com.partisiablockchain.crypto.bls;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

/** Test. Element was generated with python. */
public final class Fq6Test {

  @Test
  public void create() {
    Assertions.assertThatThrownBy(() -> Fq6.create(new byte[1]))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot create Fq6 element from 1 bytes. Expected 288");
  }

  @Test
  public void serialize() {
    Fq6 element = randomFq6();
    byte[] serialize = element.serialize();
    Assertions.assertThat(Fq6.create(serialize)).isEqualTo(element);
  }

  @Test
  public void add() {
    Fq6 element = randomFq6();
    Fq6 negate = element.negate();
    Assertions.assertThat(negate.add(element)).isEqualTo(Fq6.createZero());
  }

  @Test
  public void subtract() {
    Fq6 element = randomFq6();
    Assertions.assertThat(element.subtract(element)).isEqualTo(Fq6.createZero());
  }

  @Test
  public void multiply() {
    Fq6 element = randomFq6();
    Fq6 invert = element.invert();
    Assertions.assertThat(invert.multiply(element)).isEqualTo(Fq6.createOne());
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(Fq6.class).withNonnullFields("a0", "a1", "a2").verify();
  }

  @Test
  public void toStringTest() {
    Assertions.assertThat(Fq6.createOne().toString())
        .isEqualTo("Fq6{Fq2{1 + 0 * X} + Fq2{0 + 0 * X} * Y + Fq2{0 + 0 * X} * Y^2}");
  }

  @Test
  public void isZero() {
    Fq6 element = randomFq6();
    Assertions.assertThat(element.isZero()).isFalse();
    Assertions.assertThat(Fq6.createOne().isZero()).isFalse();
    byte[] firstNonZeroBytes = new byte[Fq6.BYTE_SIZE];
    firstNonZeroBytes[Fq2.BYTE_SIZE - 1] = 1;
    Fq6 firstNonZero = Fq6.create(firstNonZeroBytes);
    Assertions.assertThat(firstNonZero.isZero()).isFalse();
    byte[] middleNonZeroBytes = new byte[Fq6.BYTE_SIZE];
    middleNonZeroBytes[2 * Fq2.BYTE_SIZE - 1] = 1;
    // middleNonZero = 0 + 1·Y + 0·Y^2
    Fq6 middleNonZero = Fq6.create(middleNonZeroBytes);
    Assertions.assertThat(middleNonZero.isZero()).isFalse();
    byte[] leftZeroBytes = new byte[Fq6.BYTE_SIZE];
    leftZeroBytes[Fq6.BYTE_SIZE - 1] = 1;
    // leftZero = 0 + 0·Y + 1·Y^2
    Fq6 leftZero = Fq6.create(leftZeroBytes);
    Assertions.assertThat(leftZero.isZero()).isFalse();
    Assertions.assertThat(Fq6.createZero().isZero()).isTrue();
  }

  @Test
  public void sqrt() {
    Fq6 x = randomFq6();
    Assertions.assertThatThrownBy(x::sqrt)
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Sqrt in Fq6 not supported");
  }

  @Test
  public void compare() {
    // Assert that compare induces a lexicographical ordering by computing it manually.
    int[] values = new int[] {0, 1};
    List<Fq6> elements = new ArrayList<>();
    for (int a : values) {
      for (int b : values) {
        for (int c : values) {
          elements.add(createElement(a, b, c));
        }
      }
    }
    for (int i = 0; i < elements.size(); i++) {
      Fq6 a = elements.get(i);
      for (int j = i; j < elements.size(); j++) {
        Fq6 b = elements.get(j);
        if (a.equals(b)) {
          Assertions.assertThat(a.compare(b)).isEqualTo(0);
        } else {
          Assertions.assertThat(a.compare(b)).isEqualTo(-1);
          Assertions.assertThat(b.compare(a)).isEqualTo(1);
        }
      }
    }
  }

  private Fq6 createElement(int a, int b, int c) {
    // helper for compare testing.
    return new Fq6(
        Fq2.createConstant(Fq.createConstant(a)),
        Fq2.createConstant(Fq.createConstant(b)),
        Fq2.createConstant(Fq.createConstant(c)));
  }

  /** Repeatedly tests frobenius 5 number of times on random values. */
  @RepeatedTest(5)
  public void frobenius() {
    Fq6 element = randomFq6();
    Fq6 t = randomFq6();
    t = t.frobenius();
    Assertions.assertThat(t).isNotEqualTo(element);
    Assertions.assertThat(t).isNotEqualTo(Fq6.createZero());
    Assertions.assertThat(t).isNotEqualTo(Fq6.createOne());
  }

  static Fq6 randomFq6() {
    return new Fq6(Fq2Test.randomFq2(), Fq2Test.randomFq2(), Fq2Test.randomFq2());
  }
}
