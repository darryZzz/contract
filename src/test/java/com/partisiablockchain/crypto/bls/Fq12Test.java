package com.partisiablockchain.crypto.bls;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

/** Test. Test element was generated with python. */
public final class Fq12Test {

  @Test
  public void create() {
    Fq12 element = randomFq12();
    Assertions.assertThat(Fq12.create(element.serialize())).isEqualTo(element);
    Assertions.assertThatThrownBy(() -> Fq12.create(new byte[123]))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Could not create Fq12 element from 123 bytes. Expected 576");
  }

  @Test
  public void add() {
    Fq12 element = randomFq12();
    Fq12 negate = element.negate();
    Assertions.assertThat(negate.add(element)).isEqualTo(Fq12.createZero());
  }

  @Test
  public void subtract() {
    Fq12 element = randomFq12();
    Assertions.assertThat(element.subtract(element)).isEqualTo(Fq12.createZero());
    Assertions.assertThat(Fq12.createZero().subtract(element)).isEqualTo(element.negate());
  }

  @Test
  public void multiply() {
    Fq12 element = randomFq12();
    Fq12 invert = element.invert();
    Assertions.assertThat(element.multiply(invert)).isEqualTo(Fq12.createOne());
  }

  @Test
  public void square() {
    Fq12 element = randomFq12();
    Assertions.assertThat(element.square()).isEqualTo(element.multiply(element));
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(Fq12.class).withNonnullFields("a0", "a1").verify();
  }

  @Test
  public void toStringTest() {
    Assertions.assertThat(Fq12.createOne().toString())
        .isEqualTo("Fq12{" + Fq6.createOne() + " + " + Fq6.createZero() + " * Z}");
  }

  @Test
  public void isZero() {
    Fq12 element = randomFq12();
    Assertions.assertThat(element.isZero()).isFalse();
    Assertions.assertThat(Fq12.createOne().isZero()).isFalse();
    byte[] leftZeroBytes = new byte[Fq12.BYTE_SIZE];
    leftZeroBytes[Fq12.BYTE_SIZE - 1] = 1;
    Fq12 leftZero = Fq12.create(leftZeroBytes);
    // leftZero = Z
    Assertions.assertThat(leftZero.isZero()).isFalse();
    Assertions.assertThat(Fq12.createZero().isZero()).isTrue();
  }

  @Test
  public void compare() {
    Fq12 a = new Fq12(Fq6.createOne(), Fq6.createZero());
    Fq12 b = Fq12.createZero();
    Assertions.assertThat(a.compare(b)).isEqualTo(1);
    Assertions.assertThat(b.compare(a)).isEqualTo(-1);

    Fq12 c = new Fq12(Fq6.createZero(), Fq6.createOne());
    Assertions.assertThat(a.compare(c)).isEqualTo(1);
    Assertions.assertThat(c.compare(b)).isEqualTo(1);
  }

  @Test
  public void sqrt() {
    Assertions.assertThatThrownBy(() -> randomFq12().sqrt())
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Sqrt in Fq12 not supported");
  }

  /** Repeatedly tests frobenius with random values 10 times. */
  @RepeatedTest(10)
  public void frobenius() {
    Fq12 element = randomFq12();
    Fq12 t = randomFq12();
    t = t.frobenius();
    Assertions.assertThat(t).isNotEqualTo(element);
    Assertions.assertThat(t).isNotNull();
    Assertions.assertThat(t).isNotEqualTo(Fq12.createOne());
    Assertions.assertThat(t).isNotEqualTo(Fq12.createZero());
  }

  static Fq12 randomFq12() {
    return new Fq12(Fq6Test.randomFq6(), Fq6Test.randomFq6());
  }
}
