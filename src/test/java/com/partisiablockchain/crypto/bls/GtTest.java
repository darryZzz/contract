package com.partisiablockchain.crypto.bls;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.math.BigInteger;
import java.util.Random;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class GtTest {

  Gt element = new Gt(Fq12Test.randomFq12());
  private final Random rnd = new Random(1234);

  @Test
  public void gtCreate() {
    Assertions.assertThatThrownBy(() -> new Gt(Fq12.createZero()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("0 is not a member of this group");
  }

  @Test
  public void associativity() {
    // a·(b·c) = b·(a·c)
    Gt a = randomPoint();
    Gt b = randomPoint();
    Gt c = randomPoint();
    Assertions.assertThat(a.multiply(b.multiply(c))).isEqualTo(b.multiply(a.multiply(c)));
  }

  @Test
  public void exp() {
    Gt exp3 = element.multiply(element).multiply(element);
    Assertions.assertThat(exp3).isEqualTo(element.exp(NafEncoded.create(BigInteger.valueOf(3))));
  }

  @Test
  public void square() {
    Gt square = element.multiply(element);
    Assertions.assertThat(square).isEqualTo(element.square());
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(Gt.class).withNonnullFields("value").verify();
  }

  @Test
  public void toStringTest() {
    Assertions.assertThat(Gt.createOne().toString()).isEqualTo("Gt{" + Fq12.createOne() + "}");
  }

  private Gt randomPoint() {
    NafEncoded scalar = NafEncoded.create(new BigInteger(380, rnd));
    return element.exp(scalar);
  }
}
