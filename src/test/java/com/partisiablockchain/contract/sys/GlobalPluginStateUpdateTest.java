package com.partisiablockchain.contract.sys;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class GlobalPluginStateUpdateTest {

  @Test
  public void accessors() {
    byte[] rpc = new byte[3];
    GlobalPluginStateUpdate update = GlobalPluginStateUpdate.create(rpc);
    Assertions.assertThat(update.getRpc()).isEqualTo(rpc).isNotSameAs(rpc);
  }

  @Test
  public void readWrite() {
    byte[] rpc = new byte[12];
    GlobalPluginStateUpdate deserialize =
        SafeDataInputStream.deserialize(
            GlobalPluginStateUpdate::read,
            SafeDataOutputStream.serialize(GlobalPluginStateUpdate.create(rpc)));
    Assertions.assertThat(deserialize.getRpc()).isEqualTo(rpc).isNotSameAs(rpc);
  }
}
