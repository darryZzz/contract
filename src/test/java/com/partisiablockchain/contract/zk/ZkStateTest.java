package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateVoid;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ZkStateTest {

  @Test
  public void getVariables() {
    BlockchainAddress one =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress two =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    ZkState<StateVoid, ZkClosed<StateVoid>> computationState =
        new TestZkState(
            List.of(new TestZkClosed(one), new TestZkClosed(one), new TestZkClosed(two)), null);

    Assertions.assertThat(computationState.getVariables(one)).hasSize(2);
    Assertions.assertThat(computationState.getVariables(two)).hasSize(1);
    Assertions.assertThat(
            computationState.getVariables(
                BlockchainAddress.fromString("000000000000000000000000000000000000000054")))
        .hasSize(0);
  }

  @Test
  public void getPendingInputs() {
    BlockchainAddress one =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress two =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    ZkState<StateVoid, ZkClosed<StateVoid>> computationState =
        new TestZkState(
            null, List.of(new TestZkClosed(one), new TestZkClosed(one), new TestZkClosed(two)));

    Assertions.assertThat(computationState.getPendingInputs(one)).hasSize(2);
    Assertions.assertThat(computationState.getPendingInputs(two)).hasSize(1);
    Assertions.assertThat(
            computationState.getPendingInputs(
                BlockchainAddress.fromString("000000000000000000000000000000000000000054")))
        .hasSize(0);
    Assertions.assertThat(computationState.getPendingInput(0)).isNotNull();
    Assertions.assertThat(computationState.getPendingInput(1)).isNull();
  }

  private static final class TestZkState implements ZkState<StateVoid, ZkClosed<StateVoid>> {

    private final List<ZkClosed<StateVoid>> variables;
    private final List<ZkClosed<StateVoid>> pendingInputs;

    public TestZkState(
        List<ZkClosed<StateVoid>> variables, List<ZkClosed<StateVoid>> pendingInputs) {
      this.variables = variables;
      this.pendingInputs = pendingInputs;
    }

    @Override
    public void startComputation(List<StateVoid> information) {}

    @Override
    public CalculationStatus getCalculationStatus() {
      return null;
    }

    @Override
    public ZkClosed<StateVoid> getPendingInput(int id) {
      return getPendingInputs().stream()
          .filter(stateVoidZkClosed -> stateVoidZkClosed.getId() == id)
          .findFirst()
          .orElse(null);
    }

    @Override
    public List<ZkClosed<StateVoid>> getPendingInputs() {
      return pendingInputs;
    }

    @Override
    public void deletePendingInput(int id) {}

    @Override
    public ZkClosed<StateVoid> getVariable(int id) {
      return null;
    }

    @Override
    public List<ZkClosed<StateVoid>> getVariables() {
      return variables;
    }

    @Override
    public void transferVariable(int id, BlockchainAddress newOwner) {}

    @Override
    public void deleteVariable(int id) {}

    @Override
    public void openVariables(List<Integer> ids) {}

    @Override
    public void attestData(byte[] data) {}

    @Override
    public DataAttestation getAttestation(int id) {
      return null;
    }

    @Override
    public List<Integer> getAttestationIds() {
      return null;
    }

    @Override
    public void outputComplete(int... deleteVariableIds) {}

    @Override
    public void contractDone() {}
  }
}
