package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateVoid;

/** Test. */
@Immutable
final class TestZkClosed implements ZkClosed<StateVoid> {

  private final BlockchainAddress owner;
  private final LargeByteArray openValue;
  private final int bitLength;

  public TestZkClosed(BlockchainAddress owner) {
    this(owner, new byte[0], 0);
  }

  public TestZkClosed(BlockchainAddress owner, byte[] openValue, int bitLength) {
    this.owner = owner;
    this.openValue = new LargeByteArray(openValue);
    this.bitLength = bitLength;
  }

  @Override
  public int getId() {
    return 0;
  }

  @Override
  public BlockchainAddress getOwner() {
    return owner;
  }

  @Override
  public StateVoid getInformation() {
    return null;
  }

  @Override
  public boolean isSealed() {
    return false;
  }

  @Override
  public byte[] getOpenValue() {
    return openValue.getData();
  }

  @Override
  public int getShareBitLength() {
    return bitLength;
  }
}
