package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.secata.util.ShareConversion;
import java.util.Arrays;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShareReaderTest {

  @Test
  public void readLong() {
    long[] values =
        new long[] {0, 1, Long.MAX_VALUE, Long.MIN_VALUE, -1, Integer.MAX_VALUE, Integer.MIN_VALUE};
    for (long value : values) {
      byte[] actual = ShareConversion.longToBytes(value);
      ShareReader shareReader = new ShareReader(actual, 64);
      Assertions.assertThat(shareReader.readLong(64)).isEqualTo(value);
    }
  }

  @Test
  public void readSigned() {
    long[] values = new long[] {0, 1, Integer.MAX_VALUE, Integer.MIN_VALUE};
    for (long value : values) {
      byte[] actual = ShareConversion.longToBytes(value);
      ShareReader shareReader = new ShareReader(Arrays.copyOfRange(actual, 0, Integer.BYTES), 32);
      Assertions.assertThat(shareReader.readSignedLong(32)).isEqualTo(value);
    }
  }

  @Test
  public void readLongOddBitLength() {
    byte[] bytes = ShareConversion.intToBytes(0b101);
    Assertions.assertThat(new ShareReader(bytes, 32).readLong(32)).isEqualTo(0b101);
    Assertions.assertThat(new ShareReader(bytes, 32).readLong(7)).isEqualTo(0b101);
    Assertions.assertThat(new ShareReader(bytes, 32).readLong(3)).isEqualTo(0b101);
    Assertions.assertThat(new ShareReader(bytes, 32).readLong(1)).isEqualTo(0b1);
  }

  @Test
  public void readBoolean() {
    ShareReader shareReader = new ShareReader(new byte[] {(byte) 0b11011110, 0b00000001}, 10);
    Assertions.assertThat(shareReader.readBoolean()).isFalse();
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readBoolean()).isFalse();
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readBoolean()).isTrue();

    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readBoolean()).isFalse();
  }

  @Test
  public void readBytes() {
    byte[] bytes = new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    ShareReader shareReader = new ShareReader(bytes, bytes.length * 8);
    Assertions.assertThat(shareReader.readBytes(5)).isEqualTo(Arrays.copyOfRange(bytes, 0, 5));
    Assertions.assertThat(shareReader.readBytes(5)).isEqualTo(Arrays.copyOfRange(bytes, 5, 10));
  }

  @Test
  public void illegalSharesBitLength() {
    Assertions.assertThatThrownBy(() -> new ShareReader(new byte[1], 9))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Supplied open value and bit length are inconsistent");
  }

  @Test
  public void noOpenValue() {
    Assertions.assertThatThrownBy(() -> new ShareReader(null, 8))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("Value to read must be open");
  }

  @Test
  public void illegalLongBitLength() {
    ShareReader shareReader = new ShareReader(new byte[9], 65);

    Assertions.assertThatThrownBy(() -> shareReader.readLong(0))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Illegal bit length for long. Must be between 1 and 64.");
    Assertions.assertThatThrownBy(() -> shareReader.readLong(65))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Illegal bit length for long. Must be between 1 and 64.");
  }

  @Test
  public void noMoreAvailableBits() {
    ShareReader shareReader = new ShareReader(new byte[1], 8);
    shareReader.readLong(8);
    Assertions.assertThatThrownBy(shareReader::readBoolean)
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("No more bits available");
  }

  @Test
  public void create_doesNotReturnNull() {
    byte[] value = {1};
    int shareBitLength = 1;
    ShareReader shareReader = ShareReader.create(value, shareBitLength);
    Assertions.assertThat(shareReader).isNotNull();

    boolean byteIs1 = shareReader.readBoolean();
    Assertions.assertThat(byteIs1).isTrue();
  }
}
