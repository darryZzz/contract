package com.partisiablockchain.contract.zk;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ZkClosedTest {

  @Test
  public void getByteLength() {
    Assertions.assertThat(ZkClosed.getByteLength(0)).isEqualTo(0);
    Assertions.assertThat(ZkClosed.getByteLength(7)).isEqualTo(1);
    Assertions.assertThat(ZkClosed.getByteLength(8)).isEqualTo(1);
    Assertions.assertThat(ZkClosed.getByteLength(9)).isEqualTo(2);
    Assertions.assertThat(ZkClosed.getByteLength(16)).isEqualTo(2);
    Assertions.assertThat(ZkClosed.getByteLength(17)).isEqualTo(3);
  }

  @Test
  public void shareReader() {
    TestZkClosed testZkClosed = new TestZkClosed(null, new byte[] {123}, 8);
    ShareReader reader = testZkClosed.reader();
    Assertions.assertThat(reader.readLong(8)).isEqualTo(123);
  }
}
