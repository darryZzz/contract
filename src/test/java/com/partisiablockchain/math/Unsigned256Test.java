package com.partisiablockchain.math;

/*-
 * #%L
 * contract
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Random;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class Unsigned256Test {

  private static final int bits = 254;
  private static final int COUNT = 100;
  private static final Random rnd = new Random(1234);
  private static final BigInteger MAX_VALUE = BigInteger.TWO.pow(256).subtract(BigInteger.ONE);

  @Test
  void constants() {
    assertThat(Unsigned256.ZERO).isEqualTo(Unsigned256.create(BigInteger.ZERO));
    assertThat(Unsigned256.ONE).isEqualTo(Unsigned256.create(BigInteger.ONE));
    assertThat(Unsigned256.TEN).isEqualTo(Unsigned256.create(BigInteger.TEN));
  }

  @Test
  void createNegative() {
    assertThatThrownBy(() -> Unsigned256.create(BigInteger.valueOf(-1)))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Unable to create a negative unsigned number");
    assertThatThrownBy(() -> Unsigned256.create(-1))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Unable to create a negative unsigned number");
  }

  @Test
  void createFromLong() {
    assertThat(Unsigned256.create(0)).isEqualTo(Unsigned256.create(BigInteger.ZERO));
    assertThat(Unsigned256.create(112233445566L))
        .isEqualTo(Unsigned256.create(BigInteger.valueOf(112233445566L)));
    assertThat(Unsigned256.create(Long.MAX_VALUE))
        .isEqualTo(Unsigned256.create(BigInteger.valueOf(Long.MAX_VALUE)));
  }

  @Test
  void increment() {
    Unsigned256 random = randomU256(200);
    assertThat(random.increment()).isEqualTo(random.add(Unsigned256.ONE));
    assertThatThrownBy(() -> Unsigned256.create(MAX_VALUE).increment())
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An overflow occurred");
  }

  @Test
  void decrement() {
    Unsigned256 random = randomU256(200);
    assertThat(random.decrement()).isEqualTo(random.subtract(Unsigned256.ONE));
    assertThatThrownBy(Unsigned256.ZERO::decrement)
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An underflow occurred");
  }

  @Test
  void longValueExact() {
    assertThat(0).isEqualTo(Unsigned256.ZERO.longValueExact());
    assertThat(1).isEqualTo(Unsigned256.ONE.longValueExact());
    assertThat(10).isEqualTo(Unsigned256.TEN.longValueExact());
    assertThat(Long.MAX_VALUE).isEqualTo(Unsigned256.create(Long.MAX_VALUE).longValueExact());
    assertThatThrownBy(() -> Unsigned256.create(Long.MAX_VALUE).increment().longValueExact())
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Unsigned256 out of long range");
    assertThatThrownBy(
            () -> Unsigned256.create(Long.MAX_VALUE).add(Unsigned256.create(500)).longValueExact())
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Unsigned256 out of long range");
    assertThat(Unsigned256.create(1L << 62).longValueExact()).isEqualTo(1L << 62);
    assertThatThrownBy(() -> Unsigned256.create(BigInteger.ONE.shiftLeft(63)).longValueExact())
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Unsigned256 out of long range");
    assertThatThrownBy(() -> Unsigned256.create(BigInteger.ONE.shiftLeft(64)).longValueExact())
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Unsigned256 out of long range");
  }

  /** Repeartedly tests longValueExact. */
  @RepeatedTest(COUNT)
  void longValueExactRandom() {
    BigInteger bigInteger = new BigInteger(65, rnd);
    Unsigned256 unsigned256 = Unsigned256.create(bigInteger);
    if (bigInteger.bitLength() > 63) {
      assertThatThrownBy(unsigned256::longValueExact)
          .isInstanceOf(ArithmeticException.class)
          .hasMessage("Unsigned256 out of long range");
    } else {
      assertThat(bigInteger.longValueExact()).isEqualTo(unsigned256.longValueExact());
    }
  }

  /** Repeatedly tests add with random values. */
  @RepeatedTest(COUNT)
  public void addRandom() {
    BigInteger leftBig = new BigInteger(255, rnd);
    BigInteger rightBig = new BigInteger(255, rnd);
    Unsigned256 left = Unsigned256.create(leftBig);
    Unsigned256 right = Unsigned256.create(rightBig);
    assertThat(left.add(right)).isEqualTo(Unsigned256.create(leftBig.add(rightBig)));
  }

  @Test
  public void add() {
    Unsigned256 i = Unsigned256.create(new BigInteger("ffffffffffffffff", 16));
    Unsigned256 i2 = Unsigned256.create(new BigInteger("01fffffffffffffffe", 16));
    assertThat(i.add(i)).isEqualTo(i2);

    Unsigned256 c = Unsigned256.create(new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16));
    Unsigned256 d = Unsigned256.create(new BigInteger("8000000000000080000000000000", 16));
    Unsigned256 res = Unsigned256.create(new BigInteger("1800000000000007FFFFFFFFFFFFF", 16));
    assertThat(c.add(d)).isEqualTo(res);

    BigInteger all64BitsSet = new BigInteger("FFFFFFFFFFFFFFFF", 16);
    Unsigned256 a =
        Unsigned256.create(new BigInteger(String.valueOf(Long.MAX_VALUE))).add(Unsigned256.ONE);
    Unsigned256 b = Unsigned256.create(new BigInteger(String.valueOf(Long.MAX_VALUE)));
    // No overflow into next limb
    assertThat(a.add(b)).isEqualTo(Unsigned256.create(all64BitsSet));
    // Overflow into next limb
    assertThat(a.add(Unsigned256.ONE).add(b))
        .isEqualTo(Unsigned256.create(new BigInteger("10000000000000000", 16)));
    // Adding [-1, -1, 0, 0] with [1, 0, 0, 0] -> [0, 0, 1, 0]
    Unsigned256 all128BitsSet =
        Unsigned256.create(BigInteger.ONE.shiftLeft(128).subtract(BigInteger.ONE));
    assertThat(all128BitsSet.add(Unsigned256.ONE))
        .isEqualTo(Unsigned256.create(BigInteger.ONE.shiftLeft(128)));

    assertThatThrownBy(() -> Unsigned256.create(MAX_VALUE).add(Unsigned256.ONE))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An overflow occurred");
  }

  @Test
  public void addToTest() {
    long[] un = new long[] {-1, -1, -1, 0};
    long[] vn = new long[] {1, 1, 1, 0};
    assertThat(Unsigned256.addTo(un, vn, 0)).isEqualTo(0);
    assertThat(un).isEqualTo(new long[] {0, 1, 1, 1});

    long[] un2 = new long[] {-1, -1, -1};
    long[] vn2 = new long[] {1, 1, 1};
    assertThat(Unsigned256.addTo(un2, vn2, 0)).isEqualTo(1);
    assertThat(un2).isEqualTo(new long[] {0, 1, 1});
  }

  @Test
  public void addProperties() {
    Unsigned256 a = randomU256(bits);
    Unsigned256 b = randomU256(bits);
    Unsigned256 c = randomU256(bits);
    assertThat(a.add(b.add(c))).isEqualTo(a.add(b).add(c));

    Unsigned256 big = Unsigned256.create(MAX_VALUE);
    assertThat(big.add(Unsigned256.ZERO)).isEqualTo(big);

    assertThatThrownBy(() -> big.add(Unsigned256.ONE))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An overflow occurred");
  }

  /** Repeatedly tests multiply with random values. */
  @RepeatedTest(COUNT)
  public void multiplyRandom() {
    BigInteger leftBig = new BigInteger(128, rnd);
    BigInteger rightBig = new BigInteger(128, rnd);
    Unsigned256 left = Unsigned256.create(leftBig);
    Unsigned256 right = Unsigned256.create(rightBig);
    assertThat(left.multiply(right)).isEqualTo(Unsigned256.create(leftBig.multiply(rightBig)));
  }

  @Test
  public void multiplyProperties() {
    Unsigned256 a = randomU256(64);
    Unsigned256 c = randomU256(64);
    Unsigned256 b = randomU256(128);

    // a·(b·c) = (a·b)·c
    assertThat(a.multiply(b.multiply(c))).isEqualTo(a.multiply(b).multiply(c));
    // a · b = b · a
    assertThat(a.multiply(b)).isEqualTo(b.multiply(a));
    // a ·(b+c) = a·b+a·c
    assertThat(a.multiply(b.add(c))).isEqualTo(a.multiply(b).add(a.multiply(c)));

    Unsigned256 big = Unsigned256.create(MAX_VALUE);
    assertThat(big.multiply(Unsigned256.ONE)).isEqualTo(big);
    assertThat(big.multiply(Unsigned256.ZERO)).isEqualTo(Unsigned256.ZERO);

    assertThatThrownBy(() -> big.multiply(a))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An overflow occurred");
  }

  @Test
  public void ensureOverflowIsCaught() {
    Unsigned256 highestBitSet = Unsigned256.create(BigInteger.TWO.pow(255));
    Unsigned256 two = Unsigned256.create(2);
    Unsigned256 multiply = Unsigned256.ONE;
    for (int i = 0; i < 255; i++) {
      multiply = multiply.multiply(two);
      Unsigned256 finalMultiply = multiply;
      assertThatThrownBy(() -> highestBitSet.multiply(finalMultiply))
          .isInstanceOf(ArithmeticException.class)
          .hasMessage("An overflow occurred");
    }
    Unsigned256 shouldOverflow = Unsigned256.create(BigInteger.TWO.pow(255)).add(Unsigned256.ONE);
    assertThatThrownBy(() -> Unsigned256.create(BigInteger.TWO.pow(200)).multiply(shouldOverflow))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An overflow occurred");
  }

  /** Repeatedly tests multiply with random values that potentially overflow. */
  @RepeatedTest(COUNT)
  public void multiplyRandomWithOverflow() {
    int leftBitLength = rnd.nextInt(256) + 1;
    int rightBitLength = rnd.nextInt(256) + 1;
    BigInteger leftBig = new BigInteger(leftBitLength, rnd);
    BigInteger rightBig = new BigInteger(rightBitLength, rnd);
    Unsigned256 left = Unsigned256.create(leftBig);
    Unsigned256 right = Unsigned256.create(rightBig);
    BigInteger result = leftBig.multiply(rightBig);
    if (result.bitLength() > 256) {
      assertThatThrownBy(() -> left.multiply(right))
          .isInstanceOf(ArithmeticException.class)
          .hasMessage("An overflow occurred");
    } else {
      assertThat(left.multiply(right)).isEqualTo(Unsigned256.create(result));
    }
  }

  /**
   * Repeatedly adds two 64 bit integers together and check if they are added correct and the
   * overflow is detected.
   */
  @RepeatedTest(COUNT)
  public void add64() {
    BigInteger a = new BigInteger(64, rnd);
    BigInteger b = new BigInteger(64, rnd);
    long[] addRes = Unsigned256.add64(a.longValue(), b.longValue(), 0);
    assertThat(addRes[0]).isEqualTo(a.add(b).longValue());
    assertThat(addRes[1]).isEqualTo(a.add(b).shiftRight(64).longValue());

    long[] addRes2 = Unsigned256.add64(a.longValue(), b.longValue(), 1);
    assertThat(addRes2[0]).isEqualTo(a.add(b).add(BigInteger.ONE).longValue());
    assertThat(addRes2[1]).isEqualTo(a.add(b).add(BigInteger.ONE).shiftRight(64).longValue());
  }

  /**
   * Repeatedly sub two 64 bit integers together and check if they are added correct and the borrow
   * is detected.
   */
  @RepeatedTest(COUNT)
  public void sub64() {
    BigInteger a = new BigInteger(64, rnd);
    BigInteger b = new BigInteger(64, rnd);
    long[] subRes = Unsigned256.sub64(a.longValue(), b.longValue(), 0);
    BigInteger bigSubRes = a.subtract(b);
    assertThat(subRes[0]).isEqualTo(bigSubRes.longValue());
    // If the bigInteger has gone negative, then there has been an underflow and we should see a
    // borrow
    if (bigSubRes.signum() == -1) {
      assertThat(subRes[1]).isEqualTo(1);
    } else {
      assertThat(subRes[1]).isEqualTo(0);
    }
    subRes = Unsigned256.sub64(a.longValue(), b.longValue(), 1);
    bigSubRes = a.subtract(b).subtract(BigInteger.ONE);
    assertThat(subRes[0]).isEqualTo(bigSubRes.longValue());
    // If the bigInteger has gone negative, then there has been an underflow and we should see a
    // borrow
    if (bigSubRes.signum() == -1) {
      assertThat(subRes[1]).isEqualTo(1);
    } else {
      assertThat(subRes[1]).isEqualTo(0);
    }
  }

  @Test
  public void multiplyHigh() {
    BigInteger a = new BigInteger(64, rnd);
    BigInteger b = new BigInteger(64, rnd);
    assertThat(Unsigned256.multiplyHigh(a.longValue(), b.longValue()))
        .isEqualTo(a.multiply(b).shiftRight(64).longValue());
    BigInteger c = new BigInteger(60, rnd);
    BigInteger d = new BigInteger(60, rnd);
    assertThat(Unsigned256.multiplyHigh(c.longValue(), d.longValue()))
        .isEqualTo(c.multiply(d).shiftRight(64).longValue());
    assertThat(Unsigned256.multiplyHigh(0, a.longValue())).isEqualTo(0);
    assertThat(Unsigned256.multiplyHigh(a.longValue(), 0)).isEqualTo(0);
  }

  @Test
  public void subToMul() {
    // Handcrafted values in effort of making an overflow (-2 + 1 + 1) in subToMul
    BigInteger unBig =
        new BigInteger(
            "115792089237316195411016781537914546326278970553067108134179622333848597037055");
    Unsigned256 unValue = Unsigned256.create(unBig);
    BigInteger vnBig = new BigInteger("6277101735386680763835789423207666416102355444464034512895");
    Unsigned256 vnValue = Unsigned256.create(vnBig);
    assertThat(unValue.divide(vnValue)).isEqualTo(Unsigned256.create(unBig.divide(vnBig)));
    BigInteger unBig2 =
        new BigInteger(
            "115792089237316195417293883273301227089434195242432897623355228563449095127040");
    Unsigned256 unValue2 = Unsigned256.create(unBig2);
    BigInteger vnBig2 = new BigInteger("18446744073709551615");
    Unsigned256 vnValue2 = Unsigned256.create(vnBig2);
    assertThat(unValue2.divide(vnValue2)).isEqualTo(Unsigned256.create(unBig2.divide(vnBig2)));
    assertThat(unValue2.remainder(vnValue2))
        .isEqualTo(Unsigned256.create(unBig2.remainder(vnBig2)));
    BigInteger unBig3 =
        new BigInteger(
            "115792089237316195423570985008687907852929702298719625575994209400481361428480");
    Unsigned256 unValue3 = Unsigned256.create(unBig3);
    BigInteger vnBig3 = new BigInteger("340282366920938463463374607431768211455");
    Unsigned256 vnValue3 = Unsigned256.create(vnBig3);
    assertThat(unValue3.divide(vnValue3)).isEqualTo(Unsigned256.create(unBig3.divide(vnBig3)));
    assertThat(unValue3.remainder(vnValue3))
        .isEqualTo(Unsigned256.create(unBig3.remainder(vnBig3)));
    BigInteger unBig4 =
        new BigInteger(
            "115792089237316195417293883273301227089434195242432897623355228563449095127040");
    Unsigned256 unValue4 = Unsigned256.create(unBig4);
    BigInteger vnBig4 = new BigInteger("340282366920938463463374607431768211455");
    Unsigned256 vnValue4 = Unsigned256.create(vnBig4);
    assertThat(unValue4.divide(vnValue4)).isEqualTo(Unsigned256.create(unBig4.divide(vnBig4)));
    assertThat(unValue4.remainder(vnValue4))
        .isEqualTo(Unsigned256.create(unBig4.remainder(vnBig4)));
    BigInteger unBig5 =
        new BigInteger(
            "115792089237316195423570985008687907852589419931798688957223688908078464368639");
    Unsigned256 unValue5 = Unsigned256.create(unBig5);
    BigInteger vnBig5 =
        new BigInteger("6277101735386680763835789423207666416083908700390324961280");
    Unsigned256 vnValue5 = Unsigned256.create(vnBig5);
    assertThat(unValue5.divide(vnValue5)).isEqualTo(Unsigned256.create(unBig5.divide(vnBig5)));
    assertThat(unValue5.remainder(vnValue5))
        .isEqualTo(Unsigned256.create(unBig5.remainder(vnBig5)));
  }

  @Test
  public void divide() {
    Unsigned256 a = Unsigned256.create("500");
    Unsigned256 b = Unsigned256.create("501");
    assertThat(a.divide(b)).isEqualTo(Unsigned256.ZERO);
    assertThat(a.divide(a)).isEqualTo(Unsigned256.ONE);
    BigInteger dividendBig = new BigInteger("7261327077245885089469623957595166309114248383");
    Unsigned256 dividend = Unsigned256.create(dividendBig);
    BigInteger divisorBig = new BigInteger("3914376192838193476");
    Unsigned256 divisor = Unsigned256.create(divisorBig);
    assertThat(dividend.divide(divisor))
        .isEqualTo(Unsigned256.create(dividendBig.divide(divisorBig)));
    for (int i = 1; i <= 191; i++) {
      // Tests implementation on divide multi word.
      BigInteger bigA = new BigInteger(64 + i, rnd);
      for (int j = 2; j < 5; j++) {
        BigInteger bigB = new BigInteger(64 * j, rnd);
        Unsigned256 resBig = Unsigned256.create(bigA.divide(bigB));
        Unsigned256 res = Unsigned256.create(bigA).divide(Unsigned256.create(bigB));
        assertThat(res).isEqualTo(resBig);
      }
      // Test implementation of divide one word.
      BigInteger bigB = new BigInteger(64, rnd);
      Unsigned256 resBig = Unsigned256.create(bigA.divide(bigB));
      Unsigned256 res = Unsigned256.create(bigA).divide(Unsigned256.create(bigB));
      assertThat(res).isEqualTo(resBig);
    }
  }

  @Test
  public void divideByZeroErrors() {
    Unsigned256 a = Unsigned256.create(420);
    Unsigned256 zero = Unsigned256.ZERO;
    assertThatThrownBy(() -> a.divide(zero))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Division by zero");
    assertThatThrownBy(() -> a.remainder(zero))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("Division by zero");
  }

  @Test
  public void remainderMultiWordDivisor() {
    for (int i = 1; i <= 191; i++) {
      BigInteger a = new BigInteger(64 + i, rnd);
      for (int j = 2; j < 5; j++) {
        BigInteger c = new BigInteger(64 * j, rnd);
        Unsigned256 cc = Unsigned256.create(c);
        Unsigned256 remainder = Unsigned256.create(a).remainder(cc);
        assertThat(remainder).isEqualTo(Unsigned256.create(a.remainder(c)));
      }
    }
  }

  @Test
  public void remainder() {
    // Tests a bunch of edge cases.
    Unsigned256 max = Unsigned256.create(MAX_VALUE);
    BigInteger dividend =
        new BigInteger(
            "115792089237316195417306143237628154200301062018650100096824178476426563944447");
    BigInteger divisor =
        new BigInteger("6277101735386680763835789423207666416102355444464034512895");
    assertThat(Unsigned256.create(dividend).remainder(Unsigned256.create(divisor)))
        .isEqualTo(Unsigned256.create(dividend.remainder(divisor)));
    Unsigned256 a = Unsigned256.create("100");
    assertThat(a.remainder(Unsigned256.create("200"))).isEqualTo(a);
    assertThat(a.remainder(a)).isEqualTo(Unsigned256.ZERO);

    assertThat(max.divide(max.subtract(Unsigned256.ONE))).isEqualTo(Unsigned256.ONE);
    assertThat(max.remainder(max.subtract(Unsigned256.ONE))).isEqualTo(Unsigned256.ONE);
    assertThat(max.divide(max.subtract(Unsigned256.create(2)))).isEqualTo(Unsigned256.create(1));

    BigInteger allSetExceptMostSignificantBig =
        new BigInteger("7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
    Unsigned256 allSetExceptMostSignificant = Unsigned256.create(allSetExceptMostSignificantBig);
    assertThat(
            allSetExceptMostSignificant.remainder(
                allSetExceptMostSignificant.divide(Unsigned256.create(2)).add(Unsigned256.ONE)))
        .isEqualTo(
            Unsigned256.create(
                allSetExceptMostSignificantBig.mod(
                    allSetExceptMostSignificantBig.divide(BigInteger.TWO).add(BigInteger.ONE))));
  }

  @Test
  public void remainderOneWordDivisor() {
    for (int i = 1; i < 191; i++) {
      BigInteger a = new BigInteger(64 + i, rnd);
      BigInteger c = new BigInteger(64, rnd);
      Unsigned256 cc = Unsigned256.create(c);
      assertThat(Unsigned256.create(a).remainder(cc)).isEqualTo(Unsigned256.create(a.remainder(c)));
    }
  }

  @Test
  public void rareAddBackDivideTest() {
    BigInteger a = new BigInteger("3627763118133557876059597181828069636362133223663268140154880");
    BigInteger c =
        new BigInteger(
            "55936374483197937683704069570979487460682080229957283935273406269278805905690");
    BigInteger b = new BigInteger("5433007833973955807727");
    BigInteger d = new BigInteger("3610853057118474123510227652190292967857886094289981669376");
    Unsigned256 aa = Unsigned256.create(a);
    Unsigned256 bb = Unsigned256.create(b);
    Unsigned256 cc = Unsigned256.create(c);
    Unsigned256 dd = Unsigned256.create(d);
    assertThat(aa.divide(bb)).isEqualTo(Unsigned256.create(a.divide(b)));
    assertThat(cc.divide(dd)).isEqualTo(Unsigned256.create(c.divide(d)));
    assertThat(aa.remainder(bb)).isEqualTo(Unsigned256.create(a.remainder(b)));
    assertThat(cc.remainder(dd)).isEqualTo(Unsigned256.create(c.remainder(d)));
  }

  /** Repeatedly tests divide with random values. */
  @RepeatedTest(COUNT)
  public void divideRandom() {
    BigInteger dividendBig = new BigInteger(240, rnd);
    BigInteger divisorBig = new BigInteger(65, rnd);
    Unsigned256 dividend = Unsigned256.create(dividendBig);
    Unsigned256 divisor = Unsigned256.create(divisorBig);
    Unsigned256 result = Unsigned256.create(dividendBig.divide(divisorBig));
    assertThat(dividend.divide(divisor)).isEqualTo(result);
  }

  @Test
  public void udivrem2by1() {
    BigInteger u1u0 = new BigInteger("55340232221128654847");
    BigInteger bigD = new BigInteger("9223372039002259456");
    long d = -9223372034707292160L;
    long[] quoRem = Unsigned256.udivrem2by1(2, -1, d, Unsigned256.reciprocal2By1(d));
    assertThat(quoRem[0]).isEqualTo(u1u0.divide(bigD).longValue());
    assertThat(quoRem[1]).isEqualTo(u1u0.remainder(bigD).longValue());

    BigInteger u1u02 = new BigInteger("188359624283951013503543976287233553064");
    BigInteger bigD2 = new BigInteger("10611343425730382436");
    long[] quoRem2 =
        Unsigned256.udivrem2by1(
            -8235748380848898299L,
            -18624151369108824L,
            -7835400647979169180L,
            -4825694912509574280L);
    assertThat(quoRem2[0]).isEqualTo(u1u02.divide(bigD2).longValue());
    assertThat(quoRem2[1]).isEqualTo(u1u02.remainder(bigD2).longValue());
    long[] udivrem = Unsigned256.udivrem2by1(0, 0, 0, 0);
    assertThat(udivrem[0]).isEqualTo(2);
    assertThat(udivrem[1]).isEqualTo(0);
  }

  @Test
  public void divide2By1() {
    BigInteger u1u0 = new BigInteger("55340232221128654847");
    BigInteger v = new BigInteger("9223372039002259456");
    long[] remQuo = Unsigned256.divide2By1(2, -1, -9223372034707292160L);
    assertThat(remQuo[1]).isEqualTo(u1u0.divide(v).longValue());
    assertThat(remQuo[0]).isEqualTo(u1u0.remainder(v).longValue());

    BigInteger u1u02 = new BigInteger("46116860184273879040");
    BigInteger v2 = new BigInteger("9223372039002259456");
    long[] remQuo2 = Unsigned256.divide2By1(2, -9223372036854775808L, -9223372034707292160L);
    assertThat(remQuo2[1]).isEqualTo(u1u02.divide(v2).longValue());
    assertThat(remQuo2[0]).isEqualTo(u1u02.remainder(v2).longValue());

    BigInteger u1u03 = new BigInteger("39614081257132168796771975168");
    BigInteger v3 = new BigInteger("9223372036854775808");
    long[] remQuo3 = Unsigned256.divide2By1(2147483648L, 0, -9223372036854775808L);
    assertThat(remQuo3[1]).isEqualTo(u1u03.divide(v3).longValue());
    assertThat(remQuo3[0]).isEqualTo(u1u03.remainder(v3).longValue());

    BigInteger u1u04 = new BigInteger("39614081257136672396399345664");
    BigInteger v4 = new BigInteger("9223372036856872960");
    long[] remQuo4 = Unsigned256.divide2By1(2147483648L, 4503599627370496L, -9223372036852678656L);
    assertThat(remQuo4[1]).isEqualTo(u1u04.divide(v4).longValue());
    assertThat(remQuo4[0]).isEqualTo(u1u04.remainder(v4).longValue());

    BigInteger u1u05 = new BigInteger("18446744073709551614");
    BigInteger v5 = new BigInteger("18446744073709551615");
    long[] remQuo5 = Unsigned256.divide2By1(0, -2, -1);
    assertThat(remQuo5[1]).isEqualTo(u1u05.divide(v5).longValue());
    assertThat(remQuo5[0]).isEqualTo(u1u05.remainder(v5).longValue());

    BigInteger u1u06 = new BigInteger("158456325010081931113378349057");
    BigInteger v6 = new BigInteger("18446744071562067968");
    long[] remQuo6 = Unsigned256.divide2By1(8589934591L, 1, -2147483648L);
    assertThat(remQuo6[1]).isEqualTo(u1u06.divide(v6).longValue());
    assertThat(remQuo6[0]).isEqualTo(u1u06.remainder(v6).longValue());

    // Bogus values to catch the rhat-statement in the while loops.
    assertThat(Unsigned256.divide2By1(0L, 0L, -9134270480990953678L)[1]).isEqualTo(0);
    assertThat(Unsigned256.divide2By1(5754161235277696553L, 23L, 8592552107018701835L)[1])
        .isEqualTo(-6093541166819451539L);
  }

  @Test
  public void subtract() {
    BigInteger all64BitsSetBig = new BigInteger("FFFFFFFFFFFFFFFF", 16);
    Unsigned256 all64BitsSet = Unsigned256.create(all64BitsSetBig);
    BigInteger bigB = new BigInteger("10000000000000000", 16);
    Unsigned256 b = Unsigned256.create(bigB);
    BigInteger bigC = new BigInteger("10000000000000000", 16).shiftLeft(64);
    Unsigned256 c = Unsigned256.create(bigC);

    // Subtracting 0 - All64bits set = 1 and 1 in borrow for next limb.
    // [0, 1, 0, 0] - [-1, 0, 0, 0] = 1.
    assertThat(b.subtract(all64BitsSet))
        .isEqualTo(Unsigned256.create(bigB.subtract(all64BitsSetBig)));
    // [0, 0, 1, 0] - [-1, 0, 0, 0] = [1, -1, 0, 0]
    assertThat(c.subtract(all64BitsSet))
        .isEqualTo(Unsigned256.create(bigC.subtract(all64BitsSetBig)));

    // [-1, 0, 0, 0] - [2, -1, 0, 0] = [-1, 0, 0, 0]
    Unsigned256 i = Unsigned256.create(new BigInteger("ffffffffffffffff", 16));
    Unsigned256 i2 = Unsigned256.create(new BigInteger("01fffffffffffffffe", 16));
    assertThat(i2.subtract(i)).isEqualTo(i);

    Unsigned256 e = Unsigned256.create(new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16));
    Unsigned256 d = Unsigned256.create(new BigInteger("8000000000000080000000000000", 16));
    Unsigned256 res = Unsigned256.create(new BigInteger("7FFFFFFFFFFFFF7FFFFFFFFFFFFF", 16));
    assertThat(e.subtract(d)).isEqualTo(res);

    assertThatThrownBy(() -> Unsigned256.ZERO.subtract(Unsigned256.ONE))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An underflow occurred");
  }

  /** Repeatedly tests subtract with random values. */
  @RepeatedTest(COUNT)
  public void subtractRandom() {
    BigInteger randomLeft = new BigInteger(256, rnd);
    BigInteger randomRight = new BigInteger(200, rnd);
    Unsigned256 left = Unsigned256.create(randomLeft);
    Unsigned256 right = Unsigned256.create(randomRight);
    Unsigned256 result = Unsigned256.create(randomLeft.subtract(randomRight));
    assertThat(left.subtract(right)).isEqualTo(result);

    assertThat(left.subtract(left)).isEqualTo(Unsigned256.create(randomLeft.subtract(randomLeft)));

    assertThatThrownBy(() -> right.subtract(left))
        .isInstanceOf(ArithmeticException.class)
        .hasMessage("An underflow occurred");
  }

  @Test
  public void string() {
    assertThat(Unsigned256.create(new BigInteger("100")).toString()).isEqualTo("100");
  }

  @Test
  public void readWrite() {
    Unsigned256 a = randomU256(250);
    byte[] bytes = a.serialize();
    assertThat(bytes.length).isEqualTo(Unsigned256.BYTE_SIZE);
    Unsigned256 read = SafeDataInputStream.deserialize(Unsigned256::read, bytes);
    assertThat(read).isEqualTo(a);
    byte[] write = SafeDataOutputStream.serialize(a::write);
    assertThat(bytes).isEqualTo(write);
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(Unsigned256.class).withNonnullFields("value", "ZERO", "ONE").verify();
  }

  @Test
  public void compareTo() {
    Unsigned256 big = randomU256(255);
    Unsigned256 small = randomU256(100);
    Unsigned256 same1 = Unsigned256.create("11");
    Unsigned256 same2 = Unsigned256.create("11");
    assertThat(Unsigned256.ZERO.compareTo(Unsigned256.ONE)).isEqualTo(-1);
    assertThat(big.compareTo(small)).isEqualTo(1);
    assertThat(same1.compareTo(same2)).isEqualTo(0);
    assertThat(small.compareTo(big)).isEqualTo(-1);
  }

  @Test
  public void create() {
    BigInteger random = new BigInteger(256, rnd);
    Unsigned256 unsignedRandom = Unsigned256.create(random);
    assertThat(unsignedRandom.toString()).isEqualTo(random.toString());
    Unsigned256 a = randomU256(256);
    assertThat(Unsigned256.create(a.serialize())).isEqualTo(a);
    Unsigned256 maxValueUnsigned = Unsigned256.create(MAX_VALUE);
    assertThat(Unsigned256.create(maxValueUnsigned.serialize())).isEqualTo(maxValueUnsigned);
    assertThatThrownBy(() -> Unsigned256.create(MAX_VALUE.add(BigInteger.TWO)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot create 256 bit number with " + 33 * Byte.SIZE + " bits");
  }

  private Unsigned256 randomU256(int size) {
    BigInteger random = new BigInteger(size, rnd);
    if (random.bitLength() < size) {
      random = random.shiftLeft(size - random.bitLength());
    }
    return Unsigned256.create(random);
  }
}
